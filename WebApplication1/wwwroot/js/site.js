﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$('#myonoffswitch').on('change', function (e) {
    //$('#myonoffswitch').click(function (e) {
    var x = e.target.attributes;
    var y = x[3].ownerElement.checked;
    var item = "Dark Mode";

    if (y) {
        item = "Dark Mode";
    }
    else {
        item = "Day Mode";
    }

    $.post("/Home/SetTheme",
        {
            data: item
        });

    setTimeout("location.reload(true);", 150);
});